import Sprite from 'tsgame/Sprite';
import Canvas from 'tsgame/Canvas';

export abstract class Scene {
    canvas: Canvas;
    timeout: number;
    sprites: Array<Sprite> = new Array<Sprite>();
    paused: boolean = false;
    _interval: number = null;

    constructor(canvas: Canvas, fps: number = 30) {
        this.canvas = canvas;
        this.timeout = 1 / fps;
    }

    start(): void {
        // start the scene
        if (this._interval) {
            // clear any current interval
            clearInterval(this._interval);
        }
        this.paused = false;
        this._interval = window.setInterval((): void => this.update(), this.timeout);
    }

    stop(): void {
        // stop the scene
        clearInterval(this._interval);
        this._interval = null;
    }

    pause(): void {
        // stop updating sprites
        // but still render them
        this.paused = true;
    }

    resume(): void {
        // resume updating sprites
        this.paused = false;
    }

    addSprite(sprite: Sprite): void {
        // add sprite according to z index
        let index: number = 0;

        while (index < this.sprites.length && sprite.z >= this.sprites[index].z) {
            index++;
        }

        this.sprites.splice(index, 0, sprite);
    }

    abstract update(): void;
}

export default Scene;
