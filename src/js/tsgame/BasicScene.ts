import Scene from 'tsgame/Scene';
import Sprite from 'tsgame/Sprite';
import Canvas from 'tsgame/Canvas';

export class BasicScene extends Scene {
    constructor(canvas: Canvas, fps?: number) {
        super(canvas, fps);
    }

    update(): void {
        if (this.canvas) {
            // only update if there is a canvas
            let context: CanvasRenderingContext2D = this.canvas.context;
            this.sprites.forEach((sprite: Sprite) => {
                if (!this.paused) {
                    sprite.update();
                }
                context.save();
                    sprite.draw(context);
                context.restore();
            });
        }
    }
}

export default BasicScene;
