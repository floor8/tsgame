import utils from 'tsgame/utils';
import Vector2 from 'tsgame/Vector2';

export abstract class Sprite {
    position: Vector2 = new Vector2();
    size: Vector2 = new Vector2(50, 50);
    alpha: number = 1; // Opacity of sprite [0, 1]
    z: number = 0; // Sprites with higher z index appear on top

    constructor(args: any = null) {
    }

    draw(context: CanvasRenderingContext2D): void {
        context.moveTo(this.position.x, this.position.y);
        context.globalAlpha = this.alpha;
    }

    abstract update(dt?: number): void;
}

export default Sprite;
