import Sprite from 'tsgame/Sprite';

export class Canvas {
    domElement: HTMLCanvasElement;
    context: CanvasRenderingContext2D;

    constructor(id: string, fps: number = 30) {
        this.domElement = <HTMLCanvasElement> document.getElementById(id);
        this.context = this.domElement.getContext('2d');
    }
}

export default Canvas;
