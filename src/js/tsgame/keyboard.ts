export class Keyboard {
    keysDown: Array<boolean> = new Array<boolean>(256);
    key: Object = {
        // letters
        'a': 65,
        'b': 66,
        'c': 67,
        'd': 68,
        'e': 69,
        'f': 70,
        'g': 71,
        'h': 72,
        'i': 73,
        'j': 74,
        'k': 75,
        'l': 76,
        'm': 77,
        'n': 78,
        'o': 79,
        'p': 80,
        'q': 81,
        'r': 82,
        's': 83,
        't': 84,
        'u': 85,
        'v': 86,
        'w': 87,
        'x': 88,
        'y': 89,
        'z': 90,
        // numbers (must be accessed with array notation)
        '0': 48,
        '1': 49,
        '2': 50,
        '3': 51,
        '4': 52,
        '5': 53,
        '6': 54,
        '7': 55,
        '8': 56,
        '9': 57,
        // numbers (can be accessed with .)
        '_0': 48,
        '_1': 49,
        '_2': 50,
        '_3': 51,
        '_4': 52,
        '_5': 53,
        '_6': 54,
        '_7': 55,
        '_8': 56,
        '_9': 57,
        // arrows
        'left': 37,
        'up': 38,
        'right': 39,
        'down': 40,
        // modifiers
        'backspace': 8,
        'tab': 9,
        'enter': 13,
        'shift': 16,
        'ctrl': 17,
        'alt': 18,
        'caps': 18
    };

    constructor() {
        for (let i: number = 0; i < this.keysDown.length; i++) {
            this.keysDown[i] = false;
        }

        document.addEventListener('keydown', (e: KeyboardEvent): void => {
            let key: number = e.which;
            this.keysDown[key] = true;
        }); // keydown event

        document.addEventListener('keyup', (e: KeyboardEvent): void => {
            let key: number = e.which;
            this.keysDown[key] = false;
        }); // keyup event
    }
}

let keyboard: Keyboard = new Keyboard();
export default keyboard;
