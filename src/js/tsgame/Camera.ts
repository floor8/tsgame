import utils from 'tsgame/utils';
import Vector2 from 'tsgame/Vector2';

export class Camera {
    position: Vector2 = new Vector2();
    scale: Vector2 = new Vector2(1, 1);

    constructor(args: any = null) {
        utils.merge(this, args);
    }
}

export default Camera;
