import Sprite from 'tsgame/Sprite';
import Vector2 from 'tsgame/Vector2';
import utils from 'tsgame/utils';

export class BasicSprite extends Sprite {
    velocity: Vector2 = new Vector2();
    image: HTMLImageElement = new Image();

    constructor(args: any = null) {
        super(args);
        utils.merge(this, args);

        if (args) {
            if (args.imgSrc) {
                // extract the image from arguments
                this.image.src = args.imgSrc;
            }
        }
    }

    update(): void {
        this.position.add(this.velocity);
    }

    draw(context: CanvasRenderingContext2D): void {
        super.draw(context);
        context.drawImage(this.image, this.position.x, this.position.y, this.size.x, this.size.y);
    }
}

export default BasicSprite;
