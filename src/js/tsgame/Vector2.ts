export class Vector2 {
    x: number;
    y: number;
    angle: number = 0;
    magnitude: number = 0;

    constructor(x: number = 0, y: number = 0) {
        this.setXY(x, y);
    }

    setX(x: number): void {
        this.x = x;
        this._updateAM();
    }

    setY(y: number): void {
        this.y = y;
        this._updateAM();
    }

    setXY(x: number, y: number): void {
        this.x = x;
        this.y = y;
        this._updateAM();
    }

    setAngle(angle: number): void {
        this.angle = angle;
        this._updateXY();
    }

    setMagnitude(magnitude: number): void {
        this.magnitude = magnitude;
        this._updateXY();
    }

    setAM(angle: number, magnitude: number): void {
        this.angle = angle;
        this.magnitude = magnitude;
        this._updateXY();
    }

    add(vector: Vector2): void {
        this.setXY(this.x + vector.x, this.y + vector.y);
    }

    _updateXY(): void {
        this.x = this.magnitude * Math.cos(this.angle);
        this.y = this.magnitude * Math.sin(this.angle);
    }

    _updateAM(): void {
        this.angle = Math.atan(this.y / this.x);
        this.magnitude = Math.sqrt(this.x * this.x + this.y * this.y);
    }
}

export default Vector2;
