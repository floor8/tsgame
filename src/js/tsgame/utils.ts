export class Utils {
    merge<T>(target: T, source: any): T {
        // replace existing properties on the target with values from the source
        if (target && source) {
            // only run if the target and source are defined
            for (let prop in source) {
                // set target properties to source values
                if (target.hasOwnProperty(prop)) {
                    target[prop] = source[prop];
                }
            }
        }

        return target;
    }

    radToDeg(radians: number): number {
        // convert radians to degrees
        return radians * 180 / Math.PI;
    }

    degToRad(degrees: number): number {
        // aconvert degrees to radians
        return degrees * Math.PI / 180;
    }
}

let utils: Utils = new Utils();
export default utils;
