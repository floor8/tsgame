# TS Game
A simple game library written in TypeScript

# Dependency Setup
We will use the following file structure for this example. `src/vendor` is the bower directory specified in `.bowerrc`.

```
.
|-- src/
|   |-- requireConfig.js
|   `-- vendor/
|       `-- tsgame/
|           |-- dist/
|           |   |-- typings/
|           |   `-- ...
|           `-- ...
|-- .bowerrc
|-- bower.json
`-- tsconfig.json
```

## RequireJS Setup
This is an example of `requireConfig.js` to modularly load in components of tsgame.

```
require.config({
    paths: {},
    packages: [
        {
            name: 'tsgame',
            location: 'vendor/tsgame/dist'
        }
    ]
});
```

## tsconfig Setup
This is an example of how to setup `tsconfig.json` to read the TypeScript declaration files.

```
{
    "compilerOptions": {
        "paths": {
            "tsgame/*": ["../vendor/tsgame/dist/typings/*"]
        }
    }
}

```

This should allow you to import modules in the following fashion.

```
import Canvas from 'tsgame/Canvas'
import Sprite from 'tsgame/Sprite'
...
```
