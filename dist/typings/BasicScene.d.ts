import Scene from 'tsgame/Scene';
import Canvas from 'tsgame/Canvas';
export declare class BasicScene extends Scene {
    constructor(canvas: Canvas, fps?: number);
    update(): void;
}
export default BasicScene;
