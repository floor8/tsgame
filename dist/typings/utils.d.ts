export declare class Utils {
    merge<T>(target: T, source: any): T;
    radToDeg(radians: number): number;
    degToRad(degrees: number): number;
}
declare let utils: Utils;
export default utils;
