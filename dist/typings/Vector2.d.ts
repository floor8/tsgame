export declare class Vector2 {
    x: number;
    y: number;
    angle: number;
    magnitude: number;
    constructor(x?: number, y?: number);
    setX(x: number): void;
    setY(y: number): void;
    setXY(x: number, y: number): void;
    setAngle(angle: number): void;
    setMagnitude(magnitude: number): void;
    setAM(angle: number, magnitude: number): void;
    add(vector: Vector2): void;
    _updateXY(): void;
    _updateAM(): void;
}
export default Vector2;
