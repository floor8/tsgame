export declare class Canvas {
    domElement: HTMLCanvasElement;
    context: CanvasRenderingContext2D;
    constructor(id: string, fps?: number);
}
export default Canvas;
