import Sprite from 'tsgame/Sprite';
import Vector2 from 'tsgame/Vector2';
export declare class BasicSprite extends Sprite {
    velocity: Vector2;
    image: HTMLImageElement;
    constructor(args?: any);
    update(): void;
    draw(context: CanvasRenderingContext2D): void;
}
export default BasicSprite;
