import Vector2 from 'tsgame/Vector2';
export declare abstract class Sprite {
    position: Vector2;
    size: Vector2;
    alpha: number;
    z: number;
    constructor(args?: any);
    draw(context: CanvasRenderingContext2D): void;
    abstract update(dt?: number): void;
}
export default Sprite;
