import Sprite from 'tsgame/Sprite';
import Canvas from 'tsgame/Canvas';
export declare abstract class Scene {
    canvas: Canvas;
    timeout: number;
    sprites: Array<Sprite>;
    paused: boolean;
    _interval: number;
    constructor(canvas: Canvas, fps?: number);
    start(): void;
    stop(): void;
    pause(): void;
    resume(): void;
    addSprite(sprite: Sprite): void;
    abstract update(): void;
}
export default Scene;
