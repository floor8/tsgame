import Vector2 from 'tsgame/Vector2';
export declare class Camera {
    position: Vector2;
    scale: Vector2;
    constructor(args?: any);
}
export default Camera;
