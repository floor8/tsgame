var gulp = require('gulp');
var ts = require('gulp-typescript');
var notify = require('gulp-notify');
var minify = require('gulp-minify');
var replace = require('gulp-replace');
var clean = require('gulp-clean');
var merge = require('merge2');

var config = {
    srcPath: 'src/',
    distPath: 'dist/',
    tscPath: 'js/tsgame/',
    tscSelector: '*.ts',
    tscSrcOptions: {
        module: 'amd'
    },
    tscMinifyOptions: {
        ext: {
            src: '-debug.js',
            min: '.js'
        },
        noSource: true
    },
    tsProject: ts.createProject('tsconfig.json'),
    vendorPath: 'vendor/',
    // remove marked lines and blocks
    removeRegex: /^.*(remove:line|remove:start(.|\n)*remove:end).*$\n?/gm
};

config.copySrc = config.srcPath + '{*.*,templates/**/*.*}';
config.vendorSrc = config.srcPath + config.vendorPath + '**/*.*';
config.vendorDist = config.distPath + config.vendorPath;
config.tscSrc = config.srcPath + config.tscPath;
config.jsDist = config.distPath;
config.dtsDist = config.jsDist + 'typings';

var displayError = notify.onError(function(err) {
    return 'Error: ' + err.message;
});

gulp.task('build:clean', function() {
    return gulp.src(config.distPath, { read: false })
        .pipe(clean())
            .on('error', displayError);
});

gulp.task('tsc', ['build:clean'], function() {
    var tsResult = gulp.src([config.tscSrc + config.tscSelector])
        .pipe(config.tsProject()
            .on('error', displayError));

    return merge([
        tsResult.dts.pipe(gulp.dest(config.dtsDist)),
        tsResult.js.pipe(minify(config.tscMinifyOptions)
                .on('error', displayError))
            .pipe(gulp.dest(config.jsDist))
    ]);
});

gulp.task('tsc:watch', ['tsc'], function() {
    gulp.watch(config.tscSrc + config.tscSelector, ['tsc']);
});

gulp.task('build', ['tsc']);

gulp.task('watch', ['tsc:watch']);

gulp.task('default', ['build']);
